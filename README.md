# Varnost je težka

## Zakaj izgubljamo boj z zlonamerneži

Tridnevni praktični seminar s področja varnosti v računalniških komunikacijah.

## Povzetek

Niti polovica leta 2017 še ni minila, pa že jočemo zaradi varnostnih incidentov. Večje in zanimivejše incidente bomo na seminarju dekonstruirali, podrobneje razložil, kaj je šlo narobe in se naučili, kako bi se jim lahko (morda) izognili. Okuženi z mislimi zlonamernežev bomo raziskali lokalno omrežje in napadli nekaj starih protokolov, ki jih še vedno uporabljajo naprave IoT. Pregledali in preizkusili bomo dobre prakse za varovanje strežnikov in računalnikov. Za konec bomo skušali zavarovati preprosto spletno storitev, ki jo bomo sprogramirali v vsega nekaj vrsticah kode, nato pa ji dodali avtentikacijo, avtorizacijo, šifriranje, ... 

## Predlog vsebine

### Dan 1: 7. 6. 2017, 9-15h v PR16 na FRI: Omrežja

 * Raziskovanje lokalnega omrežja
 * Napadi v lokalnem omrežju
 * Kako zavarovati strežnik
 * Kako zavarovati moj računalnik
 * Kako varovati naprave IoT
 * Zaupanja nevredna omrežja (untrusted networks)

### Dan 2: 8. 6. 2017, 9-15h v PR16 na FRI: Varnostni mehanizmi

 * Kako zavarovati zelo preprosto spletno storitev
 * Avtentikacija
 * Avtorizacija
 * Šifriranje in certifikati
 * Preverjanje vnosa
 * Testiranje
 * Varno in nevarno programiranje

### Dan 3: 9. 6. 2017, 9-15h v P19 na FRI: Incidenti

 * Analiza varnostnih incidentov v preteklem letu (od jokanja, do demokratov in molka Interneta)
 * Slabe prakse in nevarno programiranje 

## Vsebina repozitorija

 * **prosojnice**: Prosojnice seminarja
 * **vaje**: Navodila za praktične naloge
 * **clanki**: Nekateri članki na katere se sklicujemo v prosojnicah; predpone datotek so številke, ki sovpadajo s številko reference v oglatih oklepajih

## Okolje za praktične naloge

Za izvedbo praktičnih nalog smo uporabili dva navidezna računalnika (*virtual machine*):

 1. Kali Linux (64 bit): https://www.kali.org/downloads/
 2. Ubuntu Linux 16.04 Desktop (64 bit): https://www.ubuntu.com/download/desktop

Za virtualizacijo smo uporabili Vmware Workstation Player (https://www.vmware.com/products/player/playerpro-evaluation.html). Uporabimo lahko tudi kakšno drugo platformo za virtualizacijo.

## Repozitorij za spletno storitev

```
git clone https://bitbucket.org/akrevl/ps2017service.git
```

*****

![Logos](res/logos.png)

*Naložbo sofinancirata Evropska unija iz Evropskega socialnega sklada in Republika Slovenija.*
 